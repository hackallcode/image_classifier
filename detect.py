from model import VGG16

model = VGG16(['cat', 'dog', 'panda'], 'weights/best_weights.h5')

best, all_classes = model.predict("images/test/cat.jpg")
print(f"In file cat.jpg pictured a {best}")
print(f"Probability of all variants: {all_classes}")

best, all_classes = model.predict("images/test/dog.jpg")
print(f"In file dog.jpg pictured a {best}")
print(f"Probability of all variants: {all_classes}")

best, all_classes = model.predict("images/test/panda.jpg")
print(f"In file panda.jpg pictured a {best}")
print(f"Probability of all variants: {all_classes}")
