from model import get_data_sets, VGG16

classes, train_data, valid_data = get_data_sets()
model = VGG16(classes)
model.train(train_data, valid_data)
model.plot_history(y_limits=(0.9, 1))
