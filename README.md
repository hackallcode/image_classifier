# How to train

To disable GPU you should add this code:
```python
import os
os.environ["CUDA_VISIBLE_DEVICES"] = "-1"
```

To disable rudiment logs about CPU you should export variable `TF_CPP_MIN_LOG_LEVEL=2` 

To start train just run `train.py` script:
```shell script
python train.py
```

# How to test

To test you model on test images just run `detect.py` script:
```shell script
python detect.py
```

# Links
* [keras](https://keras.io/api/applications/)
