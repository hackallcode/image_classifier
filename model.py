import os

import keras
import matplotlib.pyplot as plt
import numpy as np
from keras.applications.imagenet_utils import preprocess_input
from keras.callbacks import ModelCheckpoint, EarlyStopping
from keras.layers import Dense, Conv2D, MaxPool2D, Flatten
from keras.models import Sequential
from keras.optimizers import Adamax
from keras.preprocessing.image import ImageDataGenerator
from keras.preprocessing.image import load_img

VGG16_WEIGHTS_URL = (
    'https://github.com/fchollet/deep-learning-models/'
    'releases/download/v0.1/'
    'vgg16_weights_tf_dim_ordering_tf_kernels_notop.h5'
)


def get_data_sets(train_dir="images/train", valid_dir="images/valid"):
    img_data_gen = ImageDataGenerator(preprocessing_function=preprocess_input)
    train_data = img_data_gen.flow_from_directory(directory=train_dir, target_size=(224, 224))
    classes = [k for k in train_data.class_indices]
    valid_data = img_data_gen.flow_from_directory(directory=valid_dir, target_size=(224, 224), classes=classes)
    return classes, train_data, valid_data


class VGG16:
    def __init__(self, classes, weights='imagenet'):
        self.classes = classes

        self.model = Sequential()
        self.model.add(
            Conv2D(input_shape=(224, 224, 3), filters=64, kernel_size=(3, 3), padding="same", activation="relu"))
        self.model.add(Conv2D(filters=64, kernel_size=(3, 3), padding="same", activation="relu"))
        self.model.add(MaxPool2D(pool_size=(2, 2), strides=(2, 2)))
        self.model.add(Conv2D(filters=128, kernel_size=(3, 3), padding="same", activation="relu"))
        self.model.add(Conv2D(filters=128, kernel_size=(3, 3), padding="same", activation="relu"))
        self.model.add(MaxPool2D(pool_size=(2, 2), strides=(2, 2)))
        self.model.add(Conv2D(filters=256, kernel_size=(3, 3), padding="same", activation="relu"))
        self.model.add(Conv2D(filters=256, kernel_size=(3, 3), padding="same", activation="relu"))
        self.model.add(Conv2D(filters=256, kernel_size=(3, 3), padding="same", activation="relu"))
        self.model.add(MaxPool2D(pool_size=(2, 2), strides=(2, 2)))
        self.model.add(Conv2D(filters=512, kernel_size=(3, 3), padding="same", activation="relu"))
        self.model.add(Conv2D(filters=512, kernel_size=(3, 3), padding="same", activation="relu"))
        self.model.add(Conv2D(filters=512, kernel_size=(3, 3), padding="same", activation="relu"))
        self.model.add(MaxPool2D(pool_size=(2, 2), strides=(2, 2)))
        self.model.add(Conv2D(filters=512, kernel_size=(3, 3), padding="same", activation="relu"))
        self.model.add(Conv2D(filters=512, kernel_size=(3, 3), padding="same", activation="relu"))
        self.model.add(Conv2D(filters=512, kernel_size=(3, 3), padding="same", activation="relu"))
        self.model.add(MaxPool2D(pool_size=(2, 2), strides=(2, 2)))

        if weights == 'imagenet':
            for layer in self.model.layers:
                layer.trainable = False

            self.model.load_weights(keras.utils.get_file(
                'vgg16_weights_no_top.h5',
                VGG16_WEIGHTS_URL,
                cache_dir='.',
                cache_subdir='weights',
                file_hash='6d6bbae143d832006294945121d1f1fc'
            ))

        self.model.add(Flatten())
        self.model.add(Dense(units=4096, activation="relu"))
        self.model.add(Dense(units=4096, activation="relu"))
        self.model.add(Dense(units=len(self.classes), activation="softmax"))

        if weights is not None and weights != 'imagenet':
            self.model.load_weights(weights)

        opt = Adamax(lr=0.001)
        self.model.compile(optimizer=opt, loss=keras.losses.categorical_crossentropy, metrics=['accuracy'])

        self.history = {}

    def train(self, train_data, valid_data, epochs=100):
        os.makedirs("weights", exist_ok=True)
        checkpoint = ModelCheckpoint("weights/best_weights.h5", monitor='val_accuracy', verbose=1, save_best_only=True)
        early = EarlyStopping(monitor='val_accuracy', min_delta=0, patience=3, verbose=1, mode='auto')
        history = self.model.fit_generator(
            generator=train_data,
            validation_data=valid_data,
            epochs=epochs,
            callbacks=[checkpoint, early]
        )
        self.history = history.history

    def predict(self, img_path):
        img = load_img(img_path, target_size=(224, 224))
        img = np.asarray(img)
        img = np.expand_dims(img, axis=0)

        output = self.model.predict(img)[0]
        best = self.classes[np.argmax(output)]
        all_classes = {self.classes[i]: output[i] for i in range(len(output))}
        return best, all_classes

    def plot_history(self, y_limits):
        acc = self.history['accuracy']
        val_acc = self.history['val_accuracy']
        loss = self.history['loss']
        val_loss = self.history['val_loss']

        # Get number of epochs
        epochs = range(len(acc))

        # Plot training and validation accuracy per epoch
        plt.plot(epochs, acc)
        plt.plot(epochs, val_acc)
        plt.title('Training and validation accuracy')
        plt.ylim(y_limits)

        # Plot training and validation loss per epoch
        plt.figure()

        plt.plot(epochs, loss)
        plt.plot(epochs, val_loss)
        plt.title('Training and validation loss')

        plt.show()
